import { Injectable } from '@angular/core';
import {Http, Response} from "@angular/http";
import {Observable} from "rxjs";
import {Post} from "./post";
import "rxjs/operator/map";
import "rxjs/operator/catch"
import 'rxjs/Rx';




@Injectable()
export class PostService {

  constructor(private _http : Http) { }

  url : string = "https://jsonplaceholder.typicode.com";

  getall() : Observable<Post[]>{

    return this._http.get(this.url+'/posts')
      .map((r:Response)=>r.json() as Post[]);
  }

  show(id : number) : Observable<Post>{

    return this._http.get(this.url+'/posts/'+String(id))
      .map((r:Response)=>r.json() as Post);

  }

  update(id : number) : Observable<Post>{

    let datos = {
      userId  : 1,
      title: "titulo",
      body: "body adssdaasd"
    }

    return this._http.put(this.url+'/posts/'+String(id),datos)
      .map((r:Response)=>r.json() as Post);

  }


  save(post : Post) : Observable<Post>{


    return this._http.post(this.url+'/posts',post)
      .map((r:Response)=>r.json() as Post);

  }


}
