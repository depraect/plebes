import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {CustomValidators} from "./custom-validators";
import {PostService} from "./post.service";
import {Response} from "@angular/http";
import {Post} from "./post";
import "rxjs/operator/map";
import {switchMap} from "rxjs/operator/switchMap";
import {UsuarioService} from "./usuario.service";




class Persona {
  constructor(nombre:string,sexo:string, edad:number){
    this.nombre = nombre;
    this.sexo = sexo;
    this.edad = edad;
  }
  nombre : string;
  sexo: string;
  edad: number;

}






@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})


export class AppComponent implements OnInit{


  public title : string;
  public empleados : string[];
  showMe : boolean = true;
  form : FormGroup;

  posts : Post[] = [];



  constructor(private _fb : FormBuilder, private _postService : PostService, private _userService : UsuarioService){



  }

  ngOnInit(){

    this.form = this._fb.group({
      nombre : [null,[Validators.required, Validators.maxLength(20)]],
      sexo  : [null,[Validators.required, CustomValidators.email]],
      edad : [null, [Validators.required, CustomValidators.formatoNumero]]
    });

    this._postService.getall()
      .subscribe(d=>this.posts = d);

  }



  protected clickMe(i : number){

    let post : Post = this.posts[i];
    this._postService.show(post.id)
      .switchMap(p=>this._userService.get(p.userId))

      .subscribe(d=>{

        console.log(d)
      });





  }


  miestilo(i: number){
    return {odd: i%2};
  }

  toggle(){
    this.showMe = !this.showMe;
  }


  showDetails(i :number){

    let post : Post = this.posts[i];

    this._postService.show(post.id)
      .subscribe(r=>console.log(r));




  }


}
