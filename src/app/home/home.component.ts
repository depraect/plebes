import { Component, OnInit } from '@angular/core';
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import {Post} from "../post";
import {PostService} from "../post.service";
import {UsuarioService} from "../usuario.service";
import {CustomValidators} from "../custom-validators";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {


  public title : string;
  public empleados : string[];
  showMe : boolean = true;
  form : FormGroup;

  posts : Post[] = [];



  constructor(private _fb : FormBuilder, private _postService : PostService, private _userService : UsuarioService){



  }

  ngOnInit(){

    this.form = this._fb.group({
      nombre : [null,[Validators.required, Validators.maxLength(20)]],
      sexo  : [null,[Validators.required, CustomValidators.email]],
      edad : [null, [Validators.required, CustomValidators.formatoNumero]]
    });

    this._postService.getall()
      .subscribe(d=>this.posts = d);

  }



  protected clickMe(i : number){

    let post : Post = this.posts[i];
    this._postService.show(post.id)
      .switchMap(p=>this._userService.get(p.userId))

      .subscribe(d=>{

        console.log(d)
      });





  }


  miestilo(i: number){
    return {odd: i%2};
  }

  toggle(){
    this.showMe = !this.showMe;
  }


  showDetails(i :number){

    let post : Post = this.posts[i];

    this._postService.show(post.id)
      .subscribe(r=>console.log(r));




  }


}
