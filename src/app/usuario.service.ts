import { Injectable } from '@angular/core';
import {Http, Response} from "@angular/http";
import {Observable} from "rxjs";

@Injectable()
export class UsuarioService {

  url : string = "https://jsonplaceholder.typicode.com";

  constructor(private _http : Http) { }


  get(id : number) : Observable<any>{


    return this._http.get(this.url + "/users/" + String(id))
      .map((r:Response)=>r.json());



  }

}
