import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap'

import { AppComponent } from './app.component';
import {PostService} from "./post.service";
import {UsuarioService} from "./usuario.service";
import { PostComponent } from './post/post.component';
import {RouterModule} from "@angular/router";
import {routes} from "./routing";
import { HomeComponent } from './home/home.component';

@NgModule({
  declarations: [
    AppComponent,
    PostComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    NgbModule.forRoot(),
    RouterModule.forRoot(routes),
    ReactiveFormsModule
  ],
  providers: [PostService, UsuarioService],
  bootstrap: [AppComponent]
})
export class AppModule { }
