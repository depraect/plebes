import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {PostService} from "../post.service";

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {

  constructor(private activateRoute : ActivatedRoute, private _postService :PostService ) { }

  ngOnInit() {

    let id = this.activateRoute.snapshot.params['id'];
    this._postService.show(id)
      .subscribe(d=>console.log(d));
  }

}
