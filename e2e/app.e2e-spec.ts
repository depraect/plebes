import { JdticketsPage } from './app.po';

describe('jdtickets App', function() {
  let page: JdticketsPage;

  beforeEach(() => {
    page = new JdticketsPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
